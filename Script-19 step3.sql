CREATE TABLE MedSchema.Measurements (
  measurement_id INT PRIMARY KEY,
  patient_id INT,
  measurement_date DATE NOT NULL,
  value DECIMAL(10, 2) NOT NULL,
  record_ts DATE DEFAULT CURRENT_DATE,
  CHECK (measurement_date > DATE '2000-01-01' AND value >= 0),
  FOREIGN KEY (patient_id) REFERENCES MedSchema.Patients(patient_id)
);