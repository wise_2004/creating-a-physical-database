CREATE TABLE MedSchema.Gender (
  gender_id INT PRIMARY KEY,
  gender_name VARCHAR(10) NOT NULL,
  record_ts DATE DEFAULT CURRENT_DATE,
  CHECK (gender_name IN ('Male', 'Female', 'Other'))
);