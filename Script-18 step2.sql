CREATE TABLE MedSchema.Patients (
  patient_id INT PRIMARY KEY,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  date_of_birth DATE NOT NULL,
  record_ts DATE DEFAULT CURRENT_DATE,
  CHECK (date_of_birth > DATE '2000-01-01')
);