INSERT INTO MedSchema.Patients (patient_id, first_name, last_name, gender, date_of_birth)
VALUES
  (1, 'John', 'Doe', 'Male', '1990-05-01'),
  (2, 'Jane', 'Smith', 'Female', '1985-10-15');

INSERT INTO MedSchema.Gender (gender_id, gender_name)
VALUES
  (1, 'Male'),
  (2, 'Female'),
  (3, 'Other');